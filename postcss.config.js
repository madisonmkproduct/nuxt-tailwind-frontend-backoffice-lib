// postcss.config.js
const purgecss = require('@fullhuman/postcss-purgecss')({
  // Specify the paths to all of the template files in your project
  content: ['./**/*.vue', './src/**/*.vue', './src/**/*.js', './src/**/*.ts'],
  whitelistPatterns: [/^multiselect|leaflet/],

  // Include any special characters you're using in this regular expression
  defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
})

module.exports = {
  plugins: [
    // require('postcss-import'),
    // require('@nuxtjs/tailwindcss')('tailwind.config.js'),
    require('autoprefixer'),
    ...(process.env.NODE_ENV === 'production' ? [purgecss] : []),
  ],
}
