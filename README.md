=======
# Nuxt-Tailwind-Frontend-Backoffice-Lib
This project is a port from [Vue-Velocity](https://github.com/atcha/vue-velocity.git) Admin Backoffice, to make it compatible with NuxtJS framework.

A collection of Vue/Nuxt components made to create an Admin Backoffice easily from scratch.

## Project setup
```
npm install
```