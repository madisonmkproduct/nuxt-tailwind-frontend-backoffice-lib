import Alert from './Alert.vue'
import Avatar from './Avatar.vue'
import BarChart from './BarChart.vue'
import Button from './Button.vue'
import Card from './Card.vue'
import DoughnutChart from './DoughnutChart.vue'
import Icon from './Icon.vue'
import LineChart from './LineChart.vue'
import MultipleFilter from './MultipleFilter.vue'
import Nav from './Nav.vue'
import Notification from './Notification.vue'
import SideBar from './SideBar.vue'
import Slider from './Slider.vue'
import Table from './Table.vue'
import TaskCard from './TaskCard.vue'
import TaskLane from './TaskLane.vue'
import TodoList from './TodoList.vue'

export default {
  Alert,
  Avatar,
  BarChart,
  Button,
  Card,
  DoughnutChart,
  Icon,
  LineChart,
  MultipleFilter,
  Nav,
  Notification,
  SideBar,
  Slider,
  Table,
  TaskCard,
  TaskLane,
  TodoList,
}
