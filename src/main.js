import Vue from 'vue'
import * as parentComponent from './components'

export function install(Vue) {
  for (const childComponents in parentComponent) {
    // eslint-disable-next-line import/namespace
    const components = parentComponent[childComponents]
    for (const [componentName, component] of Object.entries(components)) {
      Vue.component(componentName, component)
    }
  }
}

if (typeof window !== 'undefined' && window.Vue) {
  Vue.use({ install })
}

export default { install }
